CC = g++
BUILD_DIR = build
SRC_DIR = src
INCLUDE_DIR = includes
CFLAGS = -std=c++11 -g -I $(INCLUDE_DIR)

EXECUTABLE_FILE = utunes.out

# PATTERN RULES:

# 1- Compiling object files from correspoding cpp files
$(BUILD_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@

# 2- Updating .h files. This rule is needed so that every file only includes the header files
# it directly depends on, and doesn't have to worry about indirect header dependencies.
%.hpp:
	@touch $@


LIB_OBJECTS = \
	$(BUILD_DIR)/CommandHandler.o \
	$(BUILD_DIR)/User.o \
	$(BUILD_DIR)/UserManager.o \
	$(BUILD_DIR)/Utunes.o \
	$(BUILD_DIR)/Parser.o \
	$(BUILD_DIR)/Song.o \
	$(BUILD_DIR)/SongManager.o \
	$(BUILD_DIR)/Comment.o \
	$(BUILD_DIR)/Playlist.o \
	$(BUILD_DIR)/Filter.o

OBJECTS = $(LIB_OBJECTS) $(BUILD_DIR)/Main.o


compile: $(BUILD_DIR) $(EXECUTABLE_FILE)


$(EXECUTABLE_FILE): $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o $(EXECUTABLE_FILE)

$(BUILD_DIR):
	mkdir $(BUILD_DIR)/
# Object file entries:

$(BUILD_DIR)/CommandHandler.o: $(SRC_DIR)/CommandHandler.cpp \
	$(INCLUDE_DIR)/CommandHandler.hpp \
	$(INCLUDE_DIR)/Exception.hpp \
	$(INCLUDE_DIR)/Request.hpp

$(BUILD_DIR)/User.o: $(SRC_DIR)/User.cpp \
	$(INCLUDE_DIR)/User.hpp \
	$(INCLUDE_DIR)/Song.hpp \
	$(INCLUDE_DIR)/Exception.hpp \
	$(INCLUDE_DIR)/Playlist.hpp

$(BUILD_DIR)/UserManager.o: $(SRC_DIR)/UserManager.cpp \
	$(INCLUDE_DIR)/UserManager.hpp \
	$(INCLUDE_DIR)/User.hpp \
	$(INCLUDE_DIR)/Exception.hpp

$(BUILD_DIR)/Utunes.o: $(SRC_DIR)/Utunes.cpp \
	$(INCLUDE_DIR)/Utunes.hpp \
	$(INCLUDE_DIR)/User.hpp \
	$(INCLUDE_DIR)/UserManager.hpp \
	$(INCLUDE_DIR)/Exception.hpp \
	$(INCLUDE_DIR)/SongManager.hpp \
	$(INCLUDE_DIR)/Parser.hpp

$(BUILD_DIR)/Parser.o: $(SRC_DIR)/Parser.cpp \
	$(INCLUDE_DIR)/Parser.hpp \
	$(INCLUDE_DIR)/Exception.hpp

$(BUILD_DIR)/Song.o: $(SRC_DIR)/Song.cpp \
	$(INCLUDE_DIR)/Song.hpp \
	$(INCLUDE_DIR)/Comment.hpp

$(BUILD_DIR)/SongManager.o: $(SRC_DIR)/SongManager.cpp \
	$(INCLUDE_DIR)/SongManager.hpp \
	$(INCLUDE_DIR)/Parser.hpp \
	$(INCLUDE_DIR)/Song.hpp

$(BUILD_DIR)/Comment.o: $(SRC_DIR)/Comment.cpp \
	$(INCLUDE_DIR)/Comment.hpp

$(BUILD_DIR)/Playlist.o: $(SRC_DIR)/Playlist.cpp \
	$(INCLUDE_DIR)/Playlist.hpp \
	$(INCLUDE_DIR)/Exception.hpp \
	$(INCLUDE_DIR)/Song.hpp

$(BUILD_DIR)/Filter.o: $(SRC_DIR)/Filter.cpp \
	$(INCLUDE_DIR)/Filter.hpp \
	$(INCLUDE_DIR)/UserManager.hpp \
	$(INCLUDE_DIR)/Song.hpp \
	$(INCLUDE_DIR)/Utunes.hpp

$(BUILD_DIR)/Main.o: $(SRC_DIR)/Main.cpp \
	$(INCLUDE_DIR)/CommandHandler.hpp

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR) *.o *.out
