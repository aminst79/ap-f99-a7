#include "UserManager.hpp"

using namespace std;

UserManager::UserManager() : loggedInUser(nullptr), lastPlaylistId(1) {}

int UserManager::getPlaylistsCount(const Song::SongId &songId) const
{
    if (loggedInUser == nullptr)
        throw new PermissionDeniedException();

    int count = 0;
    for (auto pair : users)
        count += pair.second->getSongInPlaylistCount(songId);

    return count;
}

int UserManager::getLikesCount(const Song::SongId &songId) const
{
    if (loggedInUser == nullptr)
        throw new PermissionDeniedException();

    int likeCount = 0;
    for (auto pair : users)
    {
        if (pair.second->hasSongInLikes(songId))
            likeCount++;
    }

    return likeCount;
}

User *UserManager::findUser(const string &email)
{
    if (users.find(email) == users.end())
        return nullptr;

    return users[email];
}

User *UserManager::findByUsername(const std::string &username)
{
    for (auto pair : users)
    {
        if (pair.second->getUsername() == username)
            return pair.second;
    }
    return nullptr;
}

void UserManager::signup(const User &user)
{
    if (findUser(user.getEmail()))
        throw new BadRequestException();

    users.emplace(user.getEmail(), new User(user.getEmail(),
                                            user.getUsername(),
                                            user.getPassword()));
}

void UserManager::login(const std::string &email, const std::string &password)
{
    User *const foundedUser = findUser(email);
    if (foundedUser && foundedUser->passwordMatches(password))
        loggedInUser = foundedUser;
    else
        throw new BadRequestException();
}

void UserManager::logout()
{
    loggedInUser = nullptr;
}

bool UserManager::isUserLoggedIn() const
{
    return loggedInUser != nullptr;
}

void UserManager::checkLoggedIn() const
{
    if (loggedInUser == nullptr)
        throw new PermissionDeniedException();
}

string UserManager::getLoggedInUsername() const
{
    if (loggedInUser != nullptr)
        return loggedInUser->getUsername();
    else
        throw new PermissionDeniedException();
}

User *UserManager::getLoggedInUser()
{
    if (loggedInUser != nullptr)
        return loggedInUser;
    else
        throw new PermissionDeniedException();
}

void UserManager::addLikedSong(Song *song) const
{
    if (loggedInUser != nullptr)
        loggedInUser->addLikedSong(song);
    else
        throw new PermissionDeniedException();
}

void UserManager::printLikedSongs()
{
    if (loggedInUser != nullptr)
        loggedInUser->printLikedSongs();
    else
        throw new PermissionDeniedException();
}

void UserManager::deleteLikedSong(Song::SongId songId)
{
    if (loggedInUser != nullptr)
        loggedInUser->deleteLikedSong(songId);
    else
        throw new PermissionDeniedException();
}

Playlist::PlaylistId UserManager::addPlaylist(const std::string &name, const Playlist::PlaylistPrivacy &privacy)
{
    if (loggedInUser != nullptr)
    {
        Playlist::PlaylistId id = loggedInUser->addPlaylist(new Playlist(lastPlaylistId, name, privacy));
        lastPlaylistId++;
        return id;
    }
    else
        throw new PermissionDeniedException();
}

void UserManager::printPlaylists(const std::string &username)
{
    if (loggedInUser != nullptr)
    {
        if (loggedInUser->getUsername() == username)
            loggedInUser->printPlaylists();
        else
        {
            User *foundUser = findByUsername(username);
            if (foundUser)
                foundUser->printPublicPlaylists();
            else
                throw new BadRequestException();
        }
    }
    else
        throw new PermissionDeniedException();
}

void UserManager::addPlaylistSong(const Playlist::PlaylistId &playlistId, Song *song)
{
    if (loggedInUser != nullptr)
        loggedInUser->addPlaylistSong(playlistId, song);
    else
        throw new PermissionDeniedException();
}

void UserManager::deletePlaylistSong(const Playlist::PlaylistId &playlistId, const Song::SongId &songId)
{
    if (loggedInUser != nullptr)
        loggedInUser->deletePlaylistSong(playlistId, songId);
    else
        throw new PermissionDeniedException();
}

Playlist *UserManager::findPublicPlaylist(const Playlist::PlaylistId &playlistId)
{
    for (auto pair : users)
    {
        Playlist *playlist = pair.second->findPublicPlaylist(playlistId);
        if (playlist)
            return playlist;
    }
    throw new PermissionDeniedException();
}

void UserManager::printPlaylistSongs(const Playlist::PlaylistId &playlistId)
{
    if (loggedInUser != nullptr)
    {
        Playlist *playlist = findPublicPlaylist(playlistId);
        playlist->printSongs();
    }
    else
        throw new PermissionDeniedException();
}

void UserManager::printOtherUsers() const
{
    if (loggedInUser != nullptr)
    {
        std::vector<std::pair<User::Email, User *>> ordered(users.begin(), users.end());
        std::sort(ordered.begin(), ordered.end());
        vector<User *> otherUsersList;
        for (pair<User::Email, User *> inputPair : ordered)
        {
            if (inputPair.second != loggedInUser)
                otherUsersList.push_back(inputPair.second);
        }
        if (!otherUsersList.size())
        {
            cout << "Empty" << endl;
            return;
        }
        for (User *user : otherUsersList)
            user->print();
    }
    else
        throw new PermissionDeniedException();
}

void UserManager::addLikedSong(const User &user, Song *song)
{
    if (!findUser(user.getEmail()))
    {
        users.emplace(user.getEmail(), new User(user.getEmail(),
                                                user.getUsername(),
                                                user.getPassword()));
    }
    User *user_ = findUser(user.getEmail());
    user_->addLikedSong(song);
}

vector<User *> UserManager::getUsersList()
{
    vector<User *> usersList;
    for (auto pair : users)
    {
        usersList.push_back(pair.second);
    }
    return usersList;
}