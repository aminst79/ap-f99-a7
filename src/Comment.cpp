#include "Comment.hpp"

using namespace std;

Comment::Comment(const int &time, const std::string &username, const std::string &comment)
	: time(time),
	  username(username),
	  comment(comment) {}

void Comment::print()
{
	cout << time << " " << username << ": " << comment << endl;
}
