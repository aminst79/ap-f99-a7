#include "SongManager.hpp"

using namespace std;

SongManager::SongManager(const Parser::RawDataList &songsRawData)
{
    for (size_t i = 1; i < songsRawData.size(); i++)
    {
        Song *song = constructSongFromSongRawData(songsRawData[i]);
        songs.emplace(song->getId(), song);
    }
}

Song *SongManager::constructSongFromSongRawData(const Parser::RawData songRawData)
{
    constexpr int SongIdIndex = 0;
    constexpr int SongTitleIndex = 1;
    constexpr int SongArtistIndex = 2;
    constexpr int SongReleaseYearIndex = 3;
    constexpr int SongLinkIndex = 4;
    return new Song(stoi(songRawData[SongIdIndex]),
                    songRawData[SongTitleIndex],
                    songRawData[SongArtistIndex],
                    songRawData[SongReleaseYearIndex],
                    songRawData[SongLinkIndex]);
}

Song *SongManager::getSong(Song::SongId songId) const
{
    if (songs.find(songId) == songs.end())
        throw new NotFoundException();
    else
    {
        Song *song;
        for (auto pair : songs)
        {
            if (pair.first == songId)
                song = pair.second;
        }
        return song;
    }
}

std::vector<Song *> SongManager::getSongsList()
{
    vector<Song *> songsList;
    for (auto pair : songs)
    {
        songsList.push_back(pair.second);
    }
    return songsList;
}

void SongManager::printSongs(const SongFilterManager *filterManager) const
{
    vector<Song *> songsList;
    map<Song::SongId, Song *> ordered(songs.begin(), songs.end());
    transform(ordered.begin(), ordered.end(), back_inserter(songsList),
              [](pair<Song::SongId, Song *> inputPair) { return inputPair.second; });

    songsList = filterManager->filter(songsList);

    if (!songsList.size())
    {
        cout << "Empty" << endl;
        return;
    }

    for (const auto &song : songsList)
        song->printInline();
}

void SongManager::printComments(const Song::SongId &songId)
{
    if (songs.find(songId) == songs.end())
        throw new NotFoundException();

    songs[songId]->printComments();
}

void SongManager::addComment(const Song::SongId &songId, const int &time, const string &username, const string &comment)
{
    if (songs.find(songId) == songs.end())
        throw new NotFoundException();

    songs[songId]->addComment(Comment(time, username, comment));
}