#include "Parser.hpp"

using namespace std;

Parser::Parser(const string &inputFilePath)
    : inputFilePath(inputFilePath) {}

vector<string> Parser::split(string word, char separator)
{
    vector<string> result;
    stringstream ss(word);

    string str;
    while (getline(ss, str, separator))
        result.push_back(str);

    return result;
}

vector<vector<string>> Parser::parse()
{
    string line;
    vector<vector<string>> result;
    ifstream file(inputFilePath);
    if (file.fail())
        throw NotFoundException();

    while (getline(file, line, '\n'))
    {
        vector<string> words = split(line, ',');
        result.push_back(words);
    }
    file.close();
    return result;
}
