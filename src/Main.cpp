#include "CommandHandler.hpp"

int main(int argc, char const *argv[])
{
    constexpr std::size_t SONGS_FILE_PATH_INDEX = 1;
    constexpr std::size_t LIKED_SONGS_FILE_PATH_INDEX = 2;

    CommandHandler commandHandler = CommandHandler(argv[SONGS_FILE_PATH_INDEX], argv[LIKED_SONGS_FILE_PATH_INDEX]);
    commandHandler.start();
    return 0;
}
