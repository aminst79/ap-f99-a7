#include "Song.hpp"

using namespace std;

Song::Song(const int &id,
		   const std::string &title,
		   const std::string &artist,
		   const std::string &releaseYear,
		   const std::string &link)
	: id(id),
	  title(title),
	  artist(artist),
	  releaseYear(releaseYear),
	  link(link) {}

Song::SongId Song::getId() const
{
	return id;
}

string Song::getArtist() const
{
	return artist;
}

string Song::getReleaseYear() const
{
	return releaseYear;
}

void Song::printInline()
{
	cout << id << " " << title << " " << artist << " " << releaseYear << endl;
}

void Song::printInlineWithConfidence(const double &confidence)
{
	cout << fixed << setprecision(2) << id << " " << confidence * 100 << "% " << title << " " << artist << " " << releaseYear << endl;
}

void Song::printMultiline(const int &playlistsCount, const int &likesCount)
{
	cout << id << endl;
	cout << title << endl;
	cout << artist << endl;
	cout << releaseYear << endl;
	cout << "#likes: " << likesCount << endl;
	cout << "#comments: " << commentList.size() << endl;
	cout << "#playlists: " << playlistsCount << endl;
}

void Song::addComment(Comment comment)
{
	commentList.push_back(comment);
}

void Song::printComments()
{
	if (!commentList.size())
	{
		cout << "Empty" << endl;
		return;
	}
	vector<Comment> commentsOrdered = commentList;
	std::sort(commentsOrdered.begin(), commentsOrdered.end());
	for (Comment comment : commentsOrdered)
		comment.print();
}