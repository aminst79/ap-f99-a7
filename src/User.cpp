#include "User.hpp"

using namespace std;

User::User(const string &email, const string &username, const string &password)
    : email(email),
      username(username),
      password(password) {}

bool User::passwordMatches(const std::string &_password) const
{
    return password == _password;
}

string User::getEmail() const
{
    return email;
}

string User::getUsername() const
{
    return username;
}

string User::getPassword() const
{
    return password;
}

int User::getPlaylistsCount()
{
    return playlists.size();
}

int User::getLikesCount()
{
    return likedSongs.size();
}

void User::print() const
{
    cout << username << endl;
}

void User::printLikedSongs() const
{
    if (!likedSongs.size())
    {
        cout << "Empty" << endl;
        return;
    }
    map<Song::SongId, Song *> ordered(likedSongs.begin(), likedSongs.end());
    for (auto pair : ordered)
    {
        pair.second->printInline();
    }
}

void User::addLikedSong(Song *song)
{
    if (likedSongs.find(song->getId()) != likedSongs.end())
        throw new BadRequestException();

    likedSongs[song->getId()] = song;
}

void User::deleteLikedSong(Song::SongId songId)
{
    if (likedSongs.find(songId) == likedSongs.end())
        throw new BadRequestException();

    likedSongs.erase(songId);
}

bool User::hasSongInLikes(const Song::SongId &songId) const
{
    return likedSongs.find(songId) != likedSongs.end();
}

int User::getSongInPlaylistCount(const Song::SongId &songId) const
{
    int count;
    for (auto pair : playlists)
    {
        if (pair.second->containsSong(songId))
            count++;
    }

    return count;
}

Playlist::PlaylistId User::addPlaylist(Playlist *playlist)
{
    playlists[playlist->getId()] = playlist;
    return playlist->getId();
}

void User::printPlaylists()
{
    if (!playlists.size())
    {
        cout << "Empty" << endl;
        return;
    }
    map<Playlist::PlaylistId, Playlist *> ordered(playlists.begin(), playlists.end());
    for (auto pair : ordered)
        pair.second->print();
}

std::vector<Playlist *> User::getPublicPlaylists()
{
    vector<Playlist *> publicPlaylists;
    map<Playlist::PlaylistId, Playlist *> ordered(playlists.begin(), playlists.end());
    for (auto pair : ordered)
    {
        if (pair.second->isPublic())
            publicPlaylists.push_back(pair.second);
    }
    return publicPlaylists;
}

void User::printPublicPlaylists()
{
    vector<Playlist *> publicPlaylists = getPublicPlaylists();
    if (!publicPlaylists.size())
    {
        cout << "Empty" << endl;
        return;
    }

    for (Playlist *playlist : publicPlaylists)
        playlist->print();
}

void User::addPlaylistSong(const Playlist::PlaylistId &playlistId, Song *song)
{
    Playlist *playlist = findPlaylist(playlistId);
    playlist->addSong(song);
}

void User::deletePlaylistSong(const Playlist::PlaylistId &playlistId, const Song::SongId &songId)
{
    Playlist *playlist = findPlaylist(playlistId);
    playlist->deleteSong(songId);
}

Playlist *User::findPublicPlaylist(const Playlist::PlaylistId &playlistId)
{
    for (auto pair : playlists)
    {
        if (pair.first == playlistId && pair.second->isPublic())
            return pair.second;
    }
    return nullptr;
}

Playlist *User::findPlaylist(const Playlist::PlaylistId &playlistId)
{
    if (playlists.find(playlistId) != playlists.end())
        return playlists[playlistId];
    else
        throw new PermissionDeniedException();
}

double User::getSimilarity(User *other, const int &songsCount)
{
    int similarCount = 0;
    for (auto pair : likedSongs)
    {
        if (other->hasSongInLikes(pair.first))
            similarCount++;
    }
    return (double)similarCount / (double)songsCount;
}