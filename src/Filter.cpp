#include "Filter.hpp"
#include "Utunes.hpp"

#include <iostream>
#include <typeinfo>

using namespace std;

vector<Song*> Filter::apply(vector<Song*> songs) const
{
    vector<Song*> result;
    std::copy_if(songs.begin(), songs.end(), std::back_inserter(result),
	 [this](Song* song){return this->accepts(song);});
	return result;
}

bool ArtistFilter::accepts(Song* song) const
{
    return this->artistName == song->getArtist();
}

ArtistFilter::ArtistFilter(string artist)
    : artistName(artist) {}

bool ReleaseDateFilter::accepts(Song* song) const
{
    return song->getReleaseYear() <= this->maxYear 
        && song->getReleaseYear() >= this->minYear;
}

ReleaseDateFilter::ReleaseDateFilter(string minYear, string maxYear)
    : minYear(minYear),
      maxYear(maxYear) {}

bool LikeFilter::accepts(Song* song) const
{
    int likeCount = this->userManager->getLikesCount(song->getId());
    return likeCount <= this->maxLike
        && likeCount >= this->minLike;
}

LikeFilter::LikeFilter(int minLike, int maxLike, UserManager* userManager)
    : minLike(minLike),
      maxLike(maxLike),
      userManager(userManager) {}

void SongFilterManager::addFilter(Filter* filter)
{
    for (size_t i = 0; i < filters.size(); i++)
    {
        if (typeid(*filters[i]) == typeid(*filter)) 
        {
			delete filters[i];
			filters[i] = filter;
			return;
		}
	}
	filters.push_back(filter);
}

vector<Song*> SongFilterManager::filter(vector<Song*> songs) const
{
    vector<Song*> result = songs;
	for (Filter* filter: filters)
		result = filter->apply(result);

	return result;
}

SongFilterManager::~SongFilterManager() {
	for (Filter* filter: filters)
		delete filter;
}