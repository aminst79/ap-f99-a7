#include "Utunes.hpp"

using namespace std;

Utunes::Utunes(const string &songsFilePath, const string &likedSongsFilePath)
    : songsParser(songsFilePath),
      likedSongsParser(likedSongsFilePath)
{
    Parser::RawDataList rawSongsData = songsParser.parse();
    Parser::RawDataList rawLikedSongsData = likedSongsParser.parse();
    importSongs(rawSongsData);
    importLikedSongs(rawLikedSongsData);
}

void Utunes::importSongs(const Parser::RawDataList &rawSongsData)
{
    songManager = SongManager(rawSongsData);
}

void Utunes::importLikedSongs(const Parser::RawDataList &rawLikedSongsData)
{
    constexpr int usernameIndex = 0;
    constexpr int emailIndex = 1;
    constexpr int passwordIndex = 2;
    constexpr int likedSongIndex = 3;

    for (size_t i = 1; i < rawLikedSongsData.size(); i++)
    {
        Parser::RawData row = rawLikedSongsData[i];
        Song *song = songManager.getSong(stoi(row[likedSongIndex]));
        userManager.addLikedSong(User(row[emailIndex], row[usernameIndex], row[passwordIndex]), song);
    }
}

void Utunes::signup(const User &user)
{
    userManager.signup(user);
    userManager.login(user.getEmail(), user.getPassword());
}

void Utunes::login(const string &email, const string &password)
{
    userManager.login(email, password);
}

void Utunes::logout()
{
    userManager.checkLoggedIn();
    userManager.logout();
    deleteFilters();
}

void Utunes::printSongs() const
{
    userManager.checkLoggedIn();
    songManager.printSongs(&songFilterManager);
}
void Utunes::printSong(const Song::SongId &songId) const
{
    Song *song = songManager.getSong(songId);
    song->printMultiline(userManager.getPlaylistsCount(songId), userManager.getLikesCount(songId));
}

void Utunes::addLikedSong(const Song::SongId songId) const
{
    Song *song = songManager.getSong(songId);
    userManager.addLikedSong(song);
}

void Utunes::deletePlaylistSong(const Playlist::PlaylistId &playlistId, const Song::SongId &songId)
{
    userManager.deletePlaylistSong(playlistId, songId);
}

void Utunes::printLikedSongs()
{
    userManager.printLikedSongs();
}

void Utunes::deleteLikedSong(const Song::SongId songId)
{
    userManager.deleteLikedSong(songId);
}

Playlist::PlaylistId Utunes::addPlaylist(const std::string name, const Playlist::PlaylistPrivacy &privacy)
{
    return userManager.addPlaylist(name, privacy);
}

void Utunes::printPlaylists(const std::string &username)
{
    userManager.printPlaylists(username);
}

void Utunes::addPlaylistSong(const Playlist::PlaylistId &playlistId, const Song::SongId &songId)
{
    Song *song = songManager.getSong(songId);
    userManager.addPlaylistSong(playlistId, song);
}

void Utunes::printPlaylistSongs(const Playlist::PlaylistId &playlistId)
{
    userManager.printPlaylistSongs(playlistId);
}

void Utunes::addFilter(const std::unordered_map<std::string, std::string> &filterObjects)
{
    userManager.checkLoggedIn();

    if (filterObjects.find("artist") != filterObjects.end())
    {
        string artist = filterObjects.at("artist");
        std::replace(artist.begin(), artist.end(), '_', ' ');
        songFilterManager.addFilter(new ArtistFilter(artist));
    }

    else if (filterObjects.find("min_year") != filterObjects.end() &&
             filterObjects.find("max_year") != filterObjects.end())
    {
        songFilterManager.addFilter(new ReleaseDateFilter(filterObjects.at("min_year"), filterObjects.at("max_year")));
    }

    else if (filterObjects.find("min_like") != filterObjects.end() &&
             filterObjects.find("max_like") != filterObjects.end())
    {
        songFilterManager.addFilter(new LikeFilter(
            Utils::extractFromString<int>(filterObjects.at("min_like")),
            Utils::extractFromString<int>(filterObjects.at("max_like")),
            &userManager));
    }

    else
        throw new BadRequestException();
}

void Utunes::deleteFilters()
{
    songFilterManager = SongFilterManager();
}

void Utunes::printComments(const Song::SongId songId)
{
    userManager.checkLoggedIn();

    songManager.printComments(songId);
}

void Utunes::addComment(const Song::SongId songId, const int &time, const std::string &comment)
{
    string username = userManager.getLoggedInUsername();
    songManager.addComment(songId, time, username, comment);
}

void Utunes::printOtherUsers() const
{
    userManager.printOtherUsers();
}

void Utunes::createUserSongMap()
{
    vector<User *> users = userManager.getUsersList();
    vector<Song *> songs = songManager.getSongsList();

    for (User *user : users)
    {
        for (Song *song : songs)
        {
            userSongMap[user][song] = user->hasSongInLikes(song->getId());
        }
    }
}

void Utunes::createSimilarityMap()
{
    vector<User *> users = userManager.getUsersList();
    const int songsCount = songManager.getSongsList().size();
    for (User *user : users)
    {
        for (User *user_ : users)
        {
            if (user != user_)
                similarityMap[user][user_] = user->getSimilarity(user_, songsCount);
        }
    }
}

void Utunes::printSimilarUsers(const int &count)
{
    createUserSongMap();
    createSimilarityMap();
    User *user = userManager.getLoggedInUser();
    int i = 0;
    auto sorted = Utils::sortByVal<User *, double>(similarityMap[user]);
    for (auto pair : sorted)
    {
        if (i < count)
            cout << fixed << setprecision(2) << pair.second * 100 << "% " << pair.first->getUsername() << endl;
        else
            break;
        i++;
    }
}

void Utunes::printRecommendedSongs(const int &count)
{
    createUserSongMap();
    createSimilarityMap();
    map<Song *, double> recommendedSongsConfidenceMap;
    vector<Song *> songs = songManager.getSongsList();
    User *user = userManager.getLoggedInUser();
    int otherUsersCount = userManager.getUsersList().size() - 1;

    for (Song *song : songs)
    {
        if (!user->hasSongInLikes(song->getId()))
        {
            double confidenceSum = 0;
            for (auto pair : userSongMap)
            {
                if (pair.second[song])
                    confidenceSum += similarityMap[pair.first][user];
            }
            recommendedSongsConfidenceMap[song] = confidenceSum / (double)otherUsersCount;
        }
    }
    int i = 0;
    auto sorted = Utils::sortByVal<Song *, double>(recommendedSongsConfidenceMap);
    for (auto pair : sorted)
    {
        if (i < count)
            pair.first->printInlineWithConfidence(pair.second);
        else
            break;
        i++;
    }
}
