#include "CommandHandler.hpp"
#include "Exception.hpp"
#include "Request.hpp"

#include <iostream>
#include <string>
#include <vector>

using namespace std;

void CommandHandler::start() { processCommands(); }

void CommandHandler::processCommands()
{
    string command;

    while (getline(cin, command, '\n'))
        runCommand(command);
}

void CommandHandler::runCommand(const string &command)
{
    try
    {
        bool notFound = false;
        const RequestType request(command);
        RequestType::Methods method = request.getMethod();
        string url = request.getRequestUrl()[0];

        if (method == RequestType::Methods::GET)
        {
            if (url == "songs")
                interface.runGetSongsCommand(request);
            else if (url == "likes")
                interface.runGetLikesCommand(request);
            else if (url == "playlists")
                interface.runGetPlaylistsCommand(request);
            else if (url == "playlists_songs")
                interface.runGetPlaylistSongsCommand(request);
            else if (url == "users")
                interface.runGetUsersCommand(request);
            else if (url == "comments")
                interface.runGetCommentsCommand(request);
            else if (url == "similar_users")
                interface.runGetSimilarUsersCommand(request);
            else if (url == "recommended")
                interface.runGetRecommendedSongs(request);
            else
                notFound = true;
        }

        else if (method == RequestType::Methods::POST)
        {
            if (url == "signup")
                interface.runSignupCommand(request);
            else if (url == "login")
                interface.runLoginCommand(request);
            else if (url == "logout")
                interface.runLogoutCommand(request);
            else if (url == "filters")
                interface.runAddFilterCommand(request);
            else if (url == "likes")
                interface.runAddLikeCommand(request);
            else if (url == "playlists")
                interface.runAddPlaylistCommand(request);
            else if (url == "playlists_songs")
                interface.runAddPlaylistSongCommand(request);
            else if (url == "comments")
                interface.runAddCommentCommand(request);
            else
                notFound = true;
        }

        else if (method == RequestType::Methods::DELETE)
        {
            if (url == "filters")
                interface.runDeleteFilterCommand(request);
            else if (url == "likes")
                interface.runDeleteLikeCommand(request);
            else if (url == "playlists_songs")
                interface.runDeletePlaylistSongCommand(request);
            else
                notFound = true;
        }
        if (notFound)
            throw new NotFoundException();
    }
    catch (Exception *exception)
    {
        cout << exception->what() << endl;
        delete exception;
    }
}