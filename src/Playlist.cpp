#include "Playlist.hpp"

using namespace std;

Playlist::Playlist(const PlaylistId &id, const std::string &name, const PlaylistPrivacy &privacy) : id(id), name(name), playlistPrivacy(privacy) {}

Playlist::PlaylistId Playlist::getId() const
{
    return id;
}

bool Playlist::isPublic() const
{
    return playlistPrivacy == PlaylistPrivacy::PUBLIC;
}

bool Playlist::containsSong(const Song::SongId &songId) const
{
    return songs.find(songId) != songs.end();
}

void Playlist::print()
{
    cout << id << " " << name << " " << getStringFromPlaylistPrivacy(playlistPrivacy) << endl;
}

void Playlist::addSong(Song *song)
{
    songs[song->getId()] = song;
}

void Playlist::deleteSong(const Song::SongId &songId)
{
    if (songs.find(songId) != songs.end())
    {
        songs.erase(songId);
    }
    else
        throw new BadRequestException();
}

void Playlist::printSongs()
{
    if (!songs.size())
    {
        cout << "Empty" << endl;
        return;
    }
    map<Song::SongId, Song *> ordered(songs.begin(), songs.end());
    for (auto pair : ordered)
        pair.second->printInline();
}