#pragma once

#include "Exception.hpp"
#include "Song.hpp"

#include <iostream>
#include <string>
#include <unordered_map>
#include <map>

class Playlist
{
public:
    using PlaylistId = int;

    enum class PlaylistPrivacy
    {
        PRIVATE,
        PUBLIC
    };
    static PlaylistPrivacy getPlaylistPrivacyFromString(const std::string &privacy)
    {
        if (privacy == "public")
            return PlaylistPrivacy::PUBLIC;
        else if (privacy == "private")
            return PlaylistPrivacy::PRIVATE;
        else
            throw new BadRequestException();
    }
    static std::string getStringFromPlaylistPrivacy(const PlaylistPrivacy &privacy)
    {
        if (privacy == PlaylistPrivacy::PUBLIC)
            return "public";
        else if (privacy == PlaylistPrivacy::PRIVATE)
            return "private";
        else
            throw new BadRequestException();
    }

    Playlist(const PlaylistId &id, const std::string &name, const PlaylistPrivacy &privacy);

    PlaylistId getId() const;
    bool isPublic() const;
    bool containsSong(const Song::SongId &songId) const;

    void print();
    void addSong(Song *song);
    void deleteSong(const Song::SongId &songId);
    void printSongs();

private:
    PlaylistId id;
    std::string name;
    std::unordered_map<Song::SongId, Song *> songs;
    PlaylistPrivacy playlistPrivacy;
};