#pragma once

#include "Comment.hpp"

#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include <iomanip>

class Song
{
public:
    using SongId = int;
    using CommentList = std::vector<Comment>;
    Song(const int &id,
         const std::string &title,
         const std::string &artist,
         const std::string &releaseYear,
         const std::string &link);

    SongId getId() const;
    std::string getArtist() const;
    std::string getReleaseYear() const;

    void printInline();
    void printInlineWithConfidence(const double &confidence);
    void printMultiline(const int &playlistsCount, const int &likesCount);

    void addComment(Comment comment);
    void printComments();

private:
    SongId id;
    CommentList commentList;
    std::string title;
    std::string artist;
    std::string releaseYear;
    std::string link;
};