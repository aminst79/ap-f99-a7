#pragma once

#include <string>
#include <iostream>

class Comment
{
public:
    Comment(const int &time, const std::string &username, const std::string &comment);

    bool operator<(const Comment &other) const
    {
        if (time == other.time)
            return username < other.username;
        return time < other.time;
    }

    void print();

private:
    int time;
    std::string username;
    std::string comment;
};