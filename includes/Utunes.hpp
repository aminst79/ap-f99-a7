#pragma once

#include "User.hpp"
#include "UserManager.hpp"
#include "SongManager.hpp"
#include "Exception.hpp"
#include "Parser.hpp"
#include "Filter.hpp"
#include "Utils.hpp"

#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>

class Utunes
{
public:
    Utunes(const std::string &songsFilePath, const std::string &likedSongsPath);

    void signup(const User &user);
    void login(const std::string &email, const std::string &password);
    void logout();

    void printSongs() const;
    void printSong(const Song::SongId &songId) const;

    void addLikedSong(const Song::SongId songId) const;
    void printLikedSongs();
    void deleteLikedSong(const Song::SongId songId);

    Playlist::PlaylistId addPlaylist(const std::string name, const Playlist::PlaylistPrivacy &privacy);
    void printPlaylists(const std::string &username);
    void addPlaylistSong(const Playlist::PlaylistId &playlistId, const Song::SongId &songId);
    void deletePlaylistSong(const Playlist::PlaylistId &playlistId, const Song::SongId &songId);
    void printPlaylistSongs(const Playlist::PlaylistId &playlistId);

    void addFilter(const std::unordered_map<std::string, std::string> &filterObjects);
    void deleteFilters();

    void printComments(const Song::SongId songId);
    void addComment(const Song::SongId songId, const int &time, const std::string &comment);

    void printOtherUsers() const;

    void printSimilarUsers(const int &count);
    void printRecommendedSongs(const int &count);

private:
    Parser songsParser;
    Parser likedSongsParser;

    SongManager songManager;
    UserManager userManager;
    SongFilterManager songFilterManager;

    std::map<User *, std::map<Song *, bool>> userSongMap;
    std::map<User *, std::map<User *, double>> similarityMap;

    void importSongs(const Parser::RawDataList &rawSongsData);
    void importLikedSongs(const Parser::RawDataList &rawLikedSongsData);

    void createUserSongMap();
    void createSimilarityMap();
};