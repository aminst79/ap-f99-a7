#pragma once

#include "Song.hpp"
#include "Exception.hpp"
#include "Playlist.hpp"

#include <string>
#include <vector>
#include <iostream>
#include <unordered_map>
#include <map>

class User
{
public:
    using Email = std::string;

    User(const std::string &email, const std::string &username, const std::string &password);

    bool operator<(const User &other) const
    {
        return username < other.username;
    }

    bool passwordMatches(const std::string &_password) const;

    std::string getEmail() const;
    std::string getUsername() const;
    std::string getPassword() const;

    int getPlaylistsCount();
    int getLikesCount();

    void print() const;

    void printLikedSongs() const;
    void addLikedSong(Song *song);
    void deleteLikedSong(Song::SongId songId);

    bool hasSongInLikes(const Song::SongId &songId) const;
    int getSongInPlaylistCount(const Song::SongId &songId) const;

    Playlist::PlaylistId addPlaylist(Playlist *playlist);
    void printPlaylists();
    void printPublicPlaylists();
    void addPlaylistSong(const Playlist::PlaylistId &playlistId, Song *song);
    void deletePlaylistSong(const Playlist::PlaylistId &playlistId, const Song::SongId &songId);
    Playlist *findPublicPlaylist(const Playlist::PlaylistId &playlistId);

    double getSimilarity(User *other, const int &songsCount);

private:
    std::unordered_map<Song::SongId, Song *> likedSongs;
    std::unordered_map<Playlist::PlaylistId, Playlist *> playlists;
    std::string email;
    std::string username;
    std::string password;

    std::vector<Playlist *> getPublicPlaylists();
    Playlist *findPlaylist(const Playlist::PlaylistId &playlistId);
};