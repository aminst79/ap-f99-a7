#pragma once

#include "Parser.hpp"
#include "Song.hpp"
#include "Filter.hpp"

#include <unordered_map>
#include <map>
#include <string>
#include <algorithm>

class SongManager
{
public:
    using SongsMap = std::unordered_map<Song::SongId, Song *>;

    SongManager() = default;
    SongManager(const Parser::RawDataList &songsRawData);

    Song *getSong(Song::SongId songId) const;
    std::vector<Song *> getSongsList();

    void printSongs(const SongFilterManager *filterManager) const;

    void printComments(const Song::SongId &songId);
    void addComment(const Song::SongId &songId, const int &time, const std::string &username, const std::string &comment);

private:
    SongsMap songs;

    Song *constructSongFromSongRawData(const Parser::RawData songRawData);
};