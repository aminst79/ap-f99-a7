#pragma once

#include <string>
#include <map>
#include <vector>
#include <algorithm>
#include <utility>

template <typename K, typename V>
bool valComperetor(const std::pair<K, V> &a,
                   const std::pair<K, V> &b)
{
    if (a.second == b.second)
        return a.first > b.first;
    return (a.second > b.second);
}

class Utils
{
public:
    template <typename ReturnValueType>
    static ReturnValueType extractFromString(const std::string &stringValue)
    {
        std::stringstream ss(stringValue);
        ReturnValueType result;
        ss >> result;
        return result;
    }
    template <typename K, typename V>
    using MapIterator = typename std::map<K, V>::const_iterator;

    template <typename K, typename V>
    static std::vector<std::pair<K, V>> sortByVal(std::map<K, V> unsorted)
    {
        std::vector<std::pair<K, V>> vec;

        for (MapIterator<K, V> iter = unsorted.begin(); iter != unsorted.end(); iter++)
        {
            vec.push_back(std::make_pair(iter->first, iter->second));
        }

        std::sort(vec.begin(), vec.end(), valComperetor<K, V>);
        return vec;
    }
};