#pragma once

#include "User.hpp"
#include "Exception.hpp"

#include <unordered_map>
#include <string>
#include <iostream>
#include <algorithm>

class UserManager
{
public:
    UserManager();

    int getPlaylistsCount(const Song::SongId &songId) const;
    int getLikesCount(const Song::SongId &songId) const;

    void signup(const User &user);
    void login(const std::string &email, const std::string &password);
    void logout();

    bool isUserLoggedIn() const;
    void checkLoggedIn() const;
    std::string getLoggedInUsername() const;
    User *getLoggedInUser();

    void addLikedSong(Song *song) const;
    void printLikedSongs();
    void deleteLikedSong(Song::SongId songId);

    Playlist::PlaylistId addPlaylist(const std::string &name, const Playlist::PlaylistPrivacy &privacy);
    void printPlaylists(const std::string &username);
    void addPlaylistSong(const Playlist::PlaylistId &playlistId, Song *song);
    void deletePlaylistSong(const Playlist::PlaylistId &playlistId, const Song::SongId &songId);
    void printPlaylistSongs(const Playlist::PlaylistId &playlistId);

    void printOtherUsers() const;

    void addLikedSong(const User &user, Song *song);
    std::vector<User *> getUsersList();

private:
    User *loggedInUser;

    std::unordered_map<User::Email, User *> users;
    Playlist::PlaylistId lastPlaylistId;

    Playlist *findPublicPlaylist(const Playlist::PlaylistId &playlistId);

    User *findUser(const std::string &email);
    User *findByUsername(const std::string &username);
};