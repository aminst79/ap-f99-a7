#pragma once

#include "Request.hpp"
#include "Interface.hpp"

#include <string>
#include <vector>

class CommandHandler
{
public:
    CommandHandler(const std::string &songsFilePath, const std::string &likedSongsFilePath) : interface(songsFilePath, likedSongsFilePath) {}

    void start();

    void processCommands();

    void runCommand(const std::string &command);

private:
    using RequestType = Request<BadRequestException, BadRequestException>;
    Interface<RequestType> interface;
};
