#pragma once

#include <vector>
#include <utility>

#include "Song.hpp"
#include "UserManager.hpp"

class Filter
{
public:
    std::vector<Song*> apply(std::vector<Song*> songs) const;
    virtual bool accepts(Song* song) const = 0;
};


class ArtistFilter : public Filter
{
public:
    virtual bool accepts(Song* song) const;
    ArtistFilter(std::string artist);
private:
    std::string artistName;
};


class ReleaseDateFilter : public Filter
{
public:
    virtual bool accepts(Song* song) const;
    ReleaseDateFilter(std::string minYear, std::string maxYear);
private:
    std::string minYear, maxYear;
};


class LikeFilter : public Filter
{
public:
    virtual bool accepts(Song* song) const;
    LikeFilter(int minLike, int maxLike, UserManager* userManager);
private:
    int minLike, maxLike;
    UserManager* userManager;
};


class SongFilterManager
{
public:
    void addFilter(Filter* filter);
    std::vector<Song*> filter(std::vector<Song*> songs) const;
    ~SongFilterManager();

private:
    std::vector<Filter*> filters;
};