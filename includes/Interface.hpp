#pragma once

#include "Utunes.hpp"
#include "Utils.hpp"

#include <string>
#include <iomanip>
#include <sstream>
#include <iostream>

template <typename RequestType>
class Interface
{
public:
    Interface(const std::string &songsFilePath, const std::string &likedSongsFilePath) : utunes(songsFilePath, likedSongsFilePath) {}

    void printSuccessMessage() { std::cout << "OK" << std::endl; }

    void runSignupCommand(const RequestType &request)
    {
        const User user(request.getParam("email"),
                        request.getParam("username"),
                        request.getParam("password"));
        utunes.signup(user);
        printSuccessMessage();
    }

    void runLoginCommand(const RequestType &request)
    {
        utunes.login(request.getParam("email"), request.getParam("password"));
        printSuccessMessage();
    }

    void runLogoutCommand(const RequestType &request)
    {
        utunes.logout();
        printSuccessMessage();
    }

    void runAddFilterCommand(const RequestType &request)
    {
        utunes.addFilter(request.getRequestParams());
        printSuccessMessage();
    }

    void runGetSongsCommand(const RequestType &request)
    {
        const auto &params = request.getRequestParams();
        if (params.find("id") != params.end())
        {
            int id = Utils::extractFromString<int>(request.getParam("id"));
            utunes.printSong(id);
        }
        else
            utunes.printSongs();
    }

    void runGetCommentsCommand(const RequestType &request)
    {
        int songId = Utils::extractFromString<int>(request.getParam("song_id"));
        utunes.printComments(songId);
    }

    void runAddCommentCommand(const RequestType &request)
    {
        int songId = Utils::extractFromString<int>(request.getParam("song_id"));
        int time = Utils::extractFromString<int>(request.getParam("time"));
        utunes.addComment(songId, time, request.getParam("comment"));
        printSuccessMessage();
    }

    void runGetUsersCommand(const RequestType &request)
    {
        utunes.printOtherUsers();
    }

    void runAddLikeCommand(const RequestType &request)
    {
        int songId = Utils::extractFromString<int>(request.getParam("id"));
        utunes.addLikedSong(songId);
        printSuccessMessage();
    }

    void runGetLikesCommand(const RequestType &request)
    {
        utunes.printLikedSongs();
    }

    void runDeleteLikeCommand(const RequestType &request)
    {
        int songId = Utils::extractFromString<int>(request.getParam("id"));
        utunes.deleteLikedSong(songId);
        printSuccessMessage();
    }

    void runAddPlaylistCommand(const RequestType &request)
    {
        Playlist::PlaylistPrivacy privacy = Playlist::getPlaylistPrivacyFromString(request.getParam("privacy"));
        Playlist::PlaylistId newId = utunes.addPlaylist(request.getParam("name"), privacy);
        std::cout << newId << std::endl;
    }

    void runGetPlaylistsCommand(const RequestType &request)
    {
        utunes.printPlaylists(request.getParam("username"));
    }

    void runAddPlaylistSongCommand(const RequestType &request)
    {
        int playlistId = Utils::extractFromString<int>(request.getParam("playlist_id"));
        int songId = Utils::extractFromString<int>(request.getParam("song_id"));
        utunes.addPlaylistSong(playlistId, songId);
        printSuccessMessage();
    }

    void runGetPlaylistSongsCommand(const RequestType &request)
    {
        int playlistId = Utils::extractFromString<int>(request.getParam("playlist_id"));
        utunes.printPlaylistSongs(playlistId);
    }

    void runDeletePlaylistSongCommand(const RequestType &request)
    {
        int playlistId = Utils::extractFromString<int>(request.getParam("playlist_id"));
        int songId = Utils::extractFromString<int>(request.getParam("song_id"));
        utunes.deletePlaylistSong(playlistId, songId);
        printSuccessMessage();
    }

    void runDeleteFilterCommand(const RequestType &request)
    {
        utunes.deleteFilters();
        printSuccessMessage();
    }

    void runGetSimilarUsersCommand(const RequestType &request)
    {
        utunes.printSimilarUsers(Utils::extractFromString<int>(request.getParam("count")));
    }

    void runGetRecommendedSongs(const RequestType &request)
    {
        utunes.printRecommendedSongs(Utils::extractFromString<int>(request.getParam("count")));
    }

private:
    Utunes utunes;
};
