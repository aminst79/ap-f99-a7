#pragma once

#include "Exception.hpp"

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <iostream>

class Parser
{
public:
    using RawData = std::vector<std::string>;
    using RawDataList = std::vector<RawData>;
    Parser(const std::string &inputFilePath);
    RawDataList parse();

private:
    std::string inputFilePath;
    std::vector<std::string> split(std::string word, char separator);
};